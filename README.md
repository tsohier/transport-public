To discover and use this code, we suggest the reader starts with the dataset and Jupyter notebook included in [this](https://archive.materialscloud.org/record/2020.87)  Materials Cloud archive where we show how to collect the data and use the main functionalities of this "transport" python library. Some changes have been made to the library since then. The usage should be largely the same, but if there are issues, one can use the version of the library included in the archive.

Minimum requirements to run the jupyter notebook:

- Python 3, Jupyter
- [AiiDA](https://www.aiida.net) version >1.0
- the transport library relies on a few standard python libraries: numpy, scipy, json, itertools, tqdm, sklearn

