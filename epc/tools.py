import numpy as np
from scipy.spatial import Delaunay
from scipy.interpolate import griddata
from .common import FD, MV, eVtoRy

def NumChi0(vals, xqs, kT, ef, occ = 'FD', dir = [1.,0.,0.], degeneracy = 2):
    """
    compute chi0 numercially in the valleys object
    careful that this is with spin degeneracy included.
    divide by 2 to go back to non-degenerate spin
    """
    dir = np.array(dir)
    if occ == "FD":
        f = FD
    elif occ == "MV":
        f = MV
    elif type(occ) == scipy.interpolate._interpolate.interp1d:
        def f(x, e, y):
            return occ(e)
    else:
        print("not implemented")
        return

    F = np.zeros(len(xqs))
    for ibnd in vals.FS.keys():
        iks = vals.FS[ibnd]['index']
        eks = vals.FS[ibnd]['energy']
        m = np.max(eks)+0.1
        ks = vals.kpoints[iks]
        fks = f(ef, eks, kT)
        for iq, q in enumerate(xqs):
            kqs = ks + q*dir
            ekqs = griddata(ks[:,:2],eks,kqs[:,:2],method='cubic', fill_value=m)
            fkqs = f(ef, ekqs, kT)
            F[iq] += degeneracy/(4*np.pi**2) * np.sum((fks-fkqs)/(ekqs-eks)) * vals.dkBZ**2 / eVtoRy
    # 2/(2pi)**2 dkBZ**2/eVtoRy = 2/(2pi)**2 *SBZ/NBZ/eVtoRy => in states/Ry/bohr**2
    # np.sum(np.ones_like(eks))/NBZ would give 1 is eks included the whole band (which they don't)
    #print(1./(2*np.pi**2)*vals.SBZ)
    return -F

def get_dos(vals, ef, degeneracy=2):
    """
    return the DOS at ef, in number of states per eV
    """
    # does not work for several nested bands
    #FStot = np.concatenate(list(vals.FS.values()))
    dos = 0
    for ibnd, fs in vals.FS.items():
        FSdum = np.column_stack((vals.kpoints[fs['index'],:2],fs['energy']))
        kpts = FSdum[:,:2]
        tri = Delaunay(kpts)

        for vertices in tri.simplices:
            tri_energies = sorted([FSdum[iv][2] for iv in vertices])
            if tri_energies[0] < ef and tri_energies[2] > ef:
                tri_kpts = np.array([ kpts[iv] for iv in vertices])
                siv = sorted(vertices, key = lambda x: FSdum[x][2])
                k1 = tri_kpts[1] - tri_kpts[0]
                k2 = tri_kpts[2] - tri_kpts[0]
                area = abs(np.cross(k1,k2))
                e10 = tri_energies[1] - tri_energies[0]
                e20 = tri_energies[2] - tri_energies[0]
                e21 = tri_energies[2] - tri_energies[1]
                if ef < tri_energies[1]:
                    dos+=  area * (ef-tri_energies[0])/e10/e20
                else:
                    dos +=  area * (tri_energies[2] -ef) /e21/e20
        #This is in number of states/eV
    return degeneracy*dos/vals.SBZ

def density_to_charge(structure, n):
    """
    from carrier density to number of charge per unit-cell
    structure: Aiida structure
    n: carrier density
    """
    area = structure.get_volume()/structure.cell[2][2] # in A^2
    area = area*1e-16 # in cm-2
    return n*area

def estimate_eF(vals, kT, ntarget):
    """
    Estimate Fermi level at temperature
    vals: valleys object
    kT : temperature in eV
    ntarget: target density in cm-2
    """
    #
    degeneracy = 1 if vals.noncolin else 2
    structure = vals.structure
    charge = density_to_charge(structure, ntarget)
    #
    eFS = np.concatenate(list(vals.FS.values()))['energy']
    mines = np.min(eFS)
    maxes = np.max(eFS)
    # first we estimate ef from the analytic formula (only works in 2D, for constant DOS)
    edge = mines if vals.charge<0 else maxes
    top = maxes if vals.charge<0 else mines
    dos = get_dos(vals, edge-np.sign(vals.charge)*0.02, degeneracy=degeneracy)
    est_eF = edge - np.sign(vals.charge) * np.log(np.exp(charge/dos/kT)-1.) * kT
    # if eF is above the edge, then replace dos at the estimated eF to get better estimate
    if (est_eF-edge)*np.sign(vals.charge) < 0 and (est_eF-top)*np.sign(vals.charge) >0 :
        dos = get_dos(vals, est_eF)
        est_eF = edge - np.sign(vals.charge)*np.log(np.exp(charge/dos/kT)-1.) * kT
    return est_eF

def n_of_e(vals,elow, ehigh, kT, occ = "FD"):
    """
    returns Fermi level and corresponding densities for
    eF between elow and ehigh.
    vals: valleys object
    kT : temperature in eV
    """
    if occ == "FD":
        f = FD
    elif occ == "MV":
        f = MV
    else:
        print("not implemented")
        return
    degeneracy = 1 if vals.noncolin else 2
    efs = np.linspace(elow, ehigh, 200)
    ns = np.zeros(efs.shape[0])
    for i,ef in enumerate(efs):
        n = 0
        for ibnd, xfs in list(vals.FS.items()):
            n += np.sum(f(ef,xfs['energy'], kT)) if vals.charge <0 else np.sum(1. - f(ef,xfs['energy'], kT))
        n *= degeneracy/vals.NBZ
        ns[i] = n
    return efs, ns

def ef_at_T(vals,kT,ntarget, occ = "FD"):
    """
    find fermi level at temperature
    vals: valleys object
    kT : temperature in eV
    ntarget: target density in cm-2
    """
    #estimation
    est_eF = estimate_eF(vals, kT, ntarget)
    kTdum = kT
    while not np.isfinite(est_eF):
        kTdum *= 1.1
        est_eF = estimate_eF(vals, kTdum, ntarget)
    structure = vals.structure
    charge = density_to_charge(structure, ntarget)
    # then we choose a window around estimate eF to look for the real eF.
    # the interval needs to be ordered as n increasing for the the np.interp function to work later
    w = 0.2
    efin = False
    # usually an interval of +/- 0.1 eV around estimated eF works,
    # but if it doesn't we increase the interval
    while not efin:
        elow = est_eF+np.sign(vals.charge)*w/2
        ehigh = est_eF-np.sign(vals.charge)*w/2
        efs, ns = n_of_e(vals,elow, ehigh, kT, occ = occ)
        if charge > np.min(ns) and charge < np.max(ns):
            efin = True
        w += 0.1
    efT  = np.interp(charge, ns,efs)
    return efT
