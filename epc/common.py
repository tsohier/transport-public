import numpy as np
from scipy.special import erf

Atobohr = 1.8897259850078587
eVtoRy = 1.0/13.605698
RytoeV = 13.605698
OhmtoRy = 1./8216.4712
RytoOhm = 8216.4712
KtoeV =  0.00008617343006013612
eC = 1.602e-19
# 1/rho/u is conductivity in e2/h, and 2*e2/h is 7.748091729...×10−5 S
u = 7.74809173e-5/2.0

def qe_nint(vec):
    return np.rint(vec + 1e-6*np.sign(vec))

def bose(kT,om):
    return 1.0/(np.exp(om/kT)-1)

def boseEA(kT, om):
    # this is defined reversed as what would make sense from the phonon point of view (om>0 for absorption), but I believe it's used only for toolbox/impurities, and definition below makes sense when looking at energy lost by initial state (energy loss <0 means absorption...). 
    if om>0:
        return 1+bose(kT, om)
    else:
        return bose(kT, abs(om))

def FD(ef, ek, kT):
    return 1.0/(1+np.exp((ek-ef)/kT))

def MV(ef, ek, kT):
    x=(ek-ef)/kT
    #xp = x - 1. / np.sqrt (2.)
    #arg = xp[:]**2
    #arg[np.argwhere(xp**2>200.)] = 200.
    # from QE   !       1/2*erf(x-1/sqrt(2)) + 1/sqrt(2*pi)*exp(-(x-1/sqrt(2))**2) + 1/2
    # xp = x - 1.0d0 / sqrt (2.0d0)
    # arg = min (maxarg, xp**2)
    # wgauss = 0.5d0 * qe_erf (xp) + 1.0d0 / sqrt (2.0d0 * pi) * exp ( -  arg) + 0.5d0
    #return 0.5 * erf(xp) + 1./ np.sqrt(2.*np.pi)*np.exp(-arg)+0.5
    return 0.5*(np.sqrt(2/ np.pi) * np.exp( -x**2 - np.sqrt(2) * x-1./2 ) + 1 - erf( x + 1./np.sqrt(2) ))

def norm2D(x):
    return np.linalg.norm(x[:2])
