import matplotlib
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from .common import FD, bose, norm2D
from scipy.spatial import Delaunay

eVtoRy = 1./13.605698066

def plot_valleys(vals):
    """
    vals : valleys object
    """
    # find the edge of the valleys
    if vals.charge < 0 :
        val,ext =  min(list(vals.extrema.items()), key = lambda x: x[1]['energy'])
    else:
        val,ext =  max(list(vals.extrema.items()), key = lambda x: x[1]['energy'])
    edge = ext['energy']
    print("zero is :", edge)
    #
    data = vals.states_for_plot('FS')
    data[:,2] = data[:,2]-edge
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(data[:,0], data[:,1], data[:,2])

    ax.set_xlabel(r'$k_x$ (bohr-1)')
    ax.set_ylabel(r'$k_y$ (bohr-1)')
    ax.set_zlabel(r'E (eV)')
    plt.show()
    return

def velocity_plot(vals, kT, efT):
    """
    vals : valleys object
    kT : temperature in eV
    efT : Fermi level at temperature in eV
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #
    if vals.charge < 0 :
        val,ext =  min(list(vals.extrema.items()), key = lambda x: x[1]['energy'])
    else:
        val,ext =  max(list(vals.extrema.items()), key = lambda x: x[1]['energy'])
    edge_energy = ext['energy']
    Edir = np.array([1.,0.])
    vals.get_FS_gradient()
    ibnd = list(vals.FS.keys())[0] if vals.charge <0 else list(vals.FS.keys())[-1]
    xs = np.linalg.norm(vals.dv[ibnd], axis = 1)
    xfs = vals.FS[ibnd]
    ys =  xfs['energy']-edge_energy
    efT -= edge_energy

    xmin, xmax = xlim = (min(xs), max(xs))
    ymin, ymax = ylim = (min(ys), max(ys))


    ax.scatter(xs, ys,s = 10, color = 'black')
    #ax.plot(ax.get_xlim(), [efT]*2, '--', c = 'black')
    ax.set_ylim(ylim)

    xe = np.linspace(ymin, ymax,100)
    X = [[4*FD(efT,e,kT)*(1-FD(efT,e,kT))] for e in xe]

    ax.imshow(X, cmap=plt.cm.Oranges,interpolation='bicubic',origin = 'lower',
              extent=(xmin, xmax, ymin, ymax), alpha=1)


    ax.set_ylabel("Energy (from edge) (eV)")
    ax.set_xlabel(r'$|\mathbf{v}|$ (ARU)')
    ax.set_aspect('auto')
    plt.show()
    return

def get_Pkkps(vals,kstr, jbnd, kT, efT):
    #
    eVtoRy = 1./13.605698
    vFSi = vals.vFSi[kstr]
    ki = vals.kirr[kstr]
    fac = 2.0*np.pi/vals.SBZ*eVtoRy
    # area stuff cancel, we end up with s-1
    FSdum = {i:np.column_stack((vals.kpoints[ks['index'],:2],ks['energy'])) for i,ks in list(vals.FS.items())}
    vi=vFSi[kstr[1]][np.array([norm2D(ki-x) for x in FSdum[kstr[1]]]).argmin()]
    #
    xfs=FSdum[jbnd]
    kpts = xfs[:,:2]
    tri = Delaunay(kpts)
    Pkkps = np.zeros(len(tri.simplices))
    geffs = np.zeros(len(tri.simplices))
    tridxs = []
    norm = 0.0
    print(list(range(3* len(vals.structure.get_positions()))))
    for mode in range(3* len(vals.structure.get_positions())):
        g2s = vals.g2s[kstr][jbnd][mode]
        omFS= vals.oms[kstr][jbnd][mode]
        outside = np.argwhere(omFS==10.).flatten()
        for sign in [1., -1.]:
            dumFS = np.column_stack((xfs[:,:2], xfs[:,2]-ki[2]+ sign * omFS))
            for i, vertices in enumerate(tri.simplices):
                if not any(j in outside for j in vertices):
                    tri_energies = sorted([dumFS[iv][2] for iv in vertices])
                    if tri_energies[0] < 0 and tri_energies[2] > 0:
                        if i not in tridxs:
                            tridxs.append(i)
                        #
                        sif = sorted(vertices, key = lambda x: dumFS[x][2])
                        tri_kpts = np.array([ kpts[iv] for iv in vertices])
                        # compute the area of the triangle
                        k1 = tri_kpts[1] - tri_kpts[0]
                        k2 = tri_kpts[2] - tri_kpts[0]
                        area = abs(np.cross(k1,k2))
                        e10 = tri_energies[1] - tri_energies[0]
                        e20 = tri_energies[2] - tri_energies[0]
                        e21 = tri_energies[2] - tri_energies[1]
                        if 0 < tri_energies[1]:
                            om = omFS[sif[0]]-0.5*tri_energies[0]*( (omFS[sif[1]]-omFS[sif[0]]) / e10 + (omFS[sif[2]]-omFS[sif[0]])/e20)
                            v = vFSi[jbnd][sif[0]]-0.5*tri_energies[0]*((vFSi[jbnd][sif[1]]-vFSi[jbnd][sif[0]])/e10+(vFSi[jbnd][sif[2]]-vFSi[jbnd][sif[0]])/e20)
                            g2 = g2s[sif[0]]-0.5*tri_energies[0]*((g2s[sif[1]]-g2s[sif[0]])/e10+(g2s[sif[2]]-g2s[sif[0]])/e20 )
                            Pfac = - area * tri_energies[0]/e10/e20
                        else:
                            om = omFS[sif[2]]-0.5*tri_energies[2]*( (omFS[sif[2]]-omFS[sif[1]]) / e21 + (omFS[sif[2]]-omFS[sif[0]])/e20)
                            g2 = g2s[sif[2]]-0.5*tri_energies[2]*((g2s[sif[2]]-g2s[sif[1]])/e21+(g2s[sif[2]]-g2s[sif[0]])/e20)
                            v = vFSi[jbnd][sif[2]]-0.5*tri_energies[2]*((vFSi[jbnd][sif[2]]-vFSi[jbnd][sif[1]])/e21+(vFSi[jbnd][sif[2]]-vFSi[jbnd][sif[0]])/e20)
                            Pfac =  area * tri_energies[2] /e21/e20
                        vfac = 1.-v/vi
                        if sign > 0:
                            Pkkps[i] += Pfac *fac* g2 * (bose(kT,om)+1) *(1-FD(efT, ki[2]-om, kT))/(1-FD(efT, ki[2], kT)) * vfac
                            geffs[i] += g2 * (bose(kT,om)+1)
                        else :
                            Pkkps[i] += Pfac * fac* g2 * bose(kT,om) *(1-FD(efT, ki[2]+om, kT))/(1-FD(efT, ki[2], kT)) * vfac
                            geffs[i] += g2 * bose(kT,om)
    #return tridxs, Pkkps
    return tri.simplices[tridxs], Pkkps[tridxs], xfs, geffs[tridxs]

def scattering_plot(vals,kstr, kT,efT):
    #
    """
    vals : valleys object
    kstr: index of the initial point to use
    kT : temperature in eV
    efT : Fermi level at temperature in eV
    Note: This is a "simplified" version missing the shading of the valleys,
    because it entails loading (and requiring) a bunch of other libraries for not
    much.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    from matplotlib.colors import LinearSegmentedColormap

    # get colormap
    ncolors = 256
    color_array = plt.get_cmap('Reds')(list(range(ncolors)))
    # change alpha values
    ntransp = 3
    color_array[:,-1] = np.array([0.0]*ntransp+[1.0]*(ncolors-ntransp))
    # create a colormap object
    cmap = LinearSegmentedColormap.from_list(name='Reds_alpha',colors=color_array)
    data = {}
    for jbnd in vals.FS:
        # get Pkkps
        simplices, Pkkps, xfs, g2effs = get_Pkkps(vals,kstr, jbnd, kT, efT)
        data[jbnd] = {'simp':simplices, 'P': Pkkps, 'fs': xfs, 'g':np.sqrt(g2effs)*1e3}

    Rytos = 4.8377687e-17
    G = sum([np.sum(data[jbnd]['P']) for jbnd in vals.FS])/Rytos*1e-15
    print("scattering time in fs", 1.0/G)
    ki = vals.kirr[kstr]
    gmax = max( [np.max(data[jbnd]['g']) for jbnd in vals.FS if len(data[jbnd]['g'])>1 ])

    l = list(vals.FS.keys()) if vals.charge<0 else reversed(list(vals.FS.keys()))
    for jbnd in l:
        geff = data[jbnd]['g']
        xfs = data[jbnd]['fs']
        norm = matplotlib.colors.Normalize(vmin=0, vmax=gmax)
        for i, idxs in enumerate(data[jbnd]['simp']):
            ax.fill(xfs[idxs,0], xfs[idxs,1],color = cmap(norm(geff[i])))
    d = 0.01
    ax.fill([ki[0] - d/2, ki[0] - d/2, ki[0]+d/2, ki[0] + d/2], [ki[1]-d/2, ki[1]+ d/2,ki[1]+ d/2,ki[1]- d/2] ,color= "black")
    #ax.scatter(ki[0],ki[1] ,color= "black")
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    cb=fig.colorbar(sm, ax=ax)
    cb.set_label(r'$g_{\rm{eff}}$ (meV)')
    ax.set_title(r'$\tau = $ '+str(1./G)[:6]+' fs')
    ax.set_xlabel(r'$k_x$ (bohr$^{-1}$)')
    ax.set_ylabel(r'$k_y$ (bohr$^{-1}$)')
    plt.show()
    return
