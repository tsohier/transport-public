from aiida.orm import load_node
from .main import Valleys
import numpy as np
from tqdm import tqdm
from .common import *

def qe_nint(vec):
    return np.rint(vec + 1e-6*np.sign(vec))

def checked_k_points_syms(nscf, bands, rots_recip,put_in_first_BZ = False, verbose = False, tol = 1e-5, track_sym = False):
    # nscf is the aiida nscf calculation
    # rots recip is a list of list of [[rot, t_rev] for rot in rotations] (result of get_qe_recip_syms)
    # return the k_point in the first BZ and equiv, which is an array
    # equiv[i]=j where j is the index of the irreducible k point to which i'th kpoint is equivalent.
    # tol = 1e-5 is QE's tolerence
    qemesh = nscf.inputs.kpoints.get_kpoints_mesh()
    nk1, nk2, nk3 = qemesh[0]
    k1, k2, k3 = qemesh[1]
    time_reversal = nscf.outputs.output_parameters.dict.time_reversal_flag
    #
    nkarr = np.array([nk1, nk2, nk3])
    karr = np.array([k1, k2, k3])
    #
    nk = nk1*nk2*nk3
    ks = np.zeros((nk,3))
    for i in range(nk1):
        for j in range(nk2):
            for k in range(nk3):
                n = k + j*nk3 + i*nk2*nk3
                ks[n][0] = float(i)/nk1+float(k1)/2/nk1
                ks[n][1] = float(j)/nk2+float(k2)/2/nk2
                ks[n][2] = float(k)/nk2+float(k3)/2/nk3
    #
    equiv=np.arange(nk)
    fromrot = np.zeros(nk, dtype=int)
    spinflip = np.ones(nk, dtype=int)
    detrots = np.array([np.linalg.det(r[0]) for r in rots_recip])
    #
    for i, k in enumerate(ks):
        if equiv[i] == i:
            for ir, (rot, trev) in enumerate(rots_recip):
                xkr = np.dot(rot, k)
                xkr = xkr - qe_nint(xkr) #np.array([x - int(round(x)) for x in xkr])
                #
                if trev==1: xkr = -xkr
                #
                vec = xkr * nkarr - 0.5*karr
                in_the_list = np.all(np.abs(vec-qe_nint(vec))<= tol)#min([ abs(vec-int(round(x)))<= 1e-5 for x in vec])
                #
                if in_the_list :
                    idxs = [int(x) for x in  np.remainder(qe_nint(xkr * nkarr - 0.5*karr + 2* nkarr), nkarr)]
                    j = int(idxs[2] + idxs[1]*nk3 + idxs[0]*nk2*nk3)
                    if j>i and equiv[j]==j:
                        equiv[j] = i
                        fromrot[j] = ir
                        spinflip[j] = detrots[ir]
                    else:
                        if equiv[j]!=i or j<i: print("something went wrong", j,i)
                if time_reversal:
                    vec = -xkr * nkarr - 0.5*karr
                    in_the_list = min(np.abs(vec-qe_nint(vec))<= tol)#min([ abs(vec-int(round(x)))<= 1e-5 for x in vec])
                    #
                    if in_the_list :
                        idxs = [int(x) for x in  np.remainder(qe_nint(-xkr * nkarr - 0.5*karr + 2* nkarr), nkarr)]
                        j = int(idxs[2] + idxs[1]*nk3 + idxs[0]*nk2*nk3)
                        if j>i and equiv[j]==j:
                            equiv[j] = i
                            fromrot[j] = ir
                            spinflip[j] = - detrots[ir]
                        else:
                            if equiv[j]!=i or j<i: print("something went wrong", j,i)
    #
    # now we check against the irr kpoints in the band
    # we build kmap, which gives the index of the kpoint in the QE irrks that is
    #equivalent to the kpoint in the irrks
    QEkps = bands.get_array('kpoints')
    QEkps[np.argwhere(QEkps[:,0]<-1e-6),0] += 1.0
    QEkps[np.argwhere(QEkps[:,1]<-1e-6),1] += 1.0
    #
    irridxs = np.argwhere(equiv==np.arange(nk)).flatten()
    irks = ks[irridxs]
    # kmap[i]=j : the i th irrk corresponds to the jth k point of QEkps
    kmap = np.zeros(len(irks),  dtype=np.int)
    for i,k in enumerate(irks):
        idxs = np.argwhere(np.linalg.norm(QEkps-k, axis=1)<1e-6).flatten()
        if idxs.shape[0] == 0:
            kmap[i] = -1
        else:
            kmap[i] = idxs[0]
    for i in np.argwhere(kmap==-1).flatten():
        ik = irridxs[i]
        k = ks[ik]
        eqks = ks[np.argwhere(equiv==ik).flatten()]
        for k in eqks:
            idxs = np.argwhere(np.linalg.norm(QEkps-k, axis=1)<1e-6).flatten()
            if idxs.shape[0]>0:
                kmap[i] = idxs[0]
        if kmap[i] ==-1: print("really not found")
    # Here we "slide to the first BZ" according to QE.
    # Actually, we're just centering the reciprocall cell around zero
    # This need to be done AFTER the above process of defining equiv,
    # otherwise I'm not sure we get the same result as QE
    ks = ks - np.rint(ks)
    # Here, using the qe_nint below will get you exactly the same irr points as qe
    # but that is not something we want, because some points on zone
    # border will be separated from the rest of the wedge.
    #np.array([k - qe_nint(k) for k in ks])
    #
    #
    #
    # Note kpoints are in bohr-1 units because of line below
    rec_cell = nscf.inputs.structure.get_ase().cell.reciprocal()*2*np.pi/Atobohr
    ks_cart = np.array([np.dot(rec_cell.T, x) for x in ks])
    if put_in_first_BZ ==True:
        bring_to_first_BZ(ks_cart, rec_cell)
    #
    if track_sym:
        return ks_cart, equiv, kmap, fromrot, spinflip
    else:
        return ks_cart, equiv, kmap

def reparse_qe_recip_syms(PW):
    try:
        ret = PW.outputs.retrieved
    except:
        ret = PW.called_descendants[-1].outputs.retrieved
    with ret.open("aiida.out") as f:
        lines = f.readlines()
    rots = []
    for i in range(len(lines)):
        if "cryst.   s(" in lines[i]:
            end = 0
            end2 = 0
            if "f =" in lines[i]:
                #print "frac. trans."
                end, end2 = 4, 3
            l1 =  np.array([float(x) for x in lines[i].split()[-4-end:-1-end]])
            l2 =  np.array([float(x) for x in lines[i+1].split()[-4-end2:-1-end2]])
            l3 =  np.array([float(x) for x in lines[i+2].split()[-4-end2:-1-end2]])
            s = np.array([l1,l2,l3])
            rots.append([s, 0.])
    return rots

def get_HO_LU(nscf, bands):
    if bands == None:
        try:
            bs = nscf.outputs.output_band
        except:
            #bs = nscf.out.remote_folder.out.remote_folder.out.output_band
            print("did not find bands")
            return
    else:
        bs = bands
    ebands = bs.get_bands()
    #
    nelec=int(round(nscf.outputs.output_parameters.dict.number_of_electrons))
    if nscf.outputs.output_parameters.dict.non_colinear_calculation:
        n_fullbands=nelec
    else:
        n_fullbands=int(nelec/2)
    #
    HO = np.max(ebands[:,n_fullbands-1])
    LU = np.min(ebands[:,n_fullbands])
    return HO, LU

def get_transp_bands(nscf, bands, DE, charge, mode = "simple", dumPW = None, reparse=True):
    #
    if dumPW is not None:
        rots = reparse_qe_recip_syms(dumPW)
    else:
        rots = reparse_qe_recip_syms(nscf)
    try:
        kpoints, equiv, kmap = checked_k_points_syms(nscf,bands, rots)
    except:
        print("Issue getting bands")
        return
    #
    ebands = bands.get_bands()[kmap]
    #
    nelec=int(round(nscf.outputs.output_parameters.dict.number_of_electrons))
    if nscf.outputs.output_parameters.dict.non_colinear_calculation:
        n_fullbands=nelec
    else:
        n_fullbands=int(nelec/2)
    #
    HO = np.max(ebands[:,n_fullbands-1])
    LU = np.min(ebands[:,n_fullbands])
    edge = HO if charge>0 else LU
    #
    if charge > 0:
        bands_idxs = range(n_fullbands)
    elif charge < 0:
        bands_idxs = range(n_fullbands, len(ebands[0]))
    transpbands = {}
    # create datatype for structured array made of integer indices and float energies
    dt = np.dtype([('index', np.int16), ('energy',np.float64)])
    #
    #print(DE, edge)
    for i in bands_idxs:
        if np.any(np.abs(ebands[:,i]-edge)<DE):
            dum = [(k, e[i]) for k,e in enumerate(ebands)]
            transpbands[i]=np.array(dum, dtype=dt)
    if mode == "simple":
        irridxs = np.where(equiv==np.arange(len(equiv)))[0]
        irrks = kpoints[irridxs]
        return {bdstr:np.array([np.array([irrks[k[0]][0], irrks[k[0]][1], k[1] ]) for k in ks]) for bdstr, ks in transpbands.items()}
    else:
        return kpoints,equiv, transpbands, rots

def get_Valleys(bands, doping,maxom,  ef_from_edge = None, nscfpk = None, optimize_BZ = True):
    """
    This defines a default energy window from the Fermi energy
    found in the nscf, or the one provided, and the maximum phonon energy specified
    """
    # In a normal case, the nscf is just the creator of the bands
    nscf = bands.creator
    try:
        params = nscf.inputs.parameters.get_dict()
    except:
        # if that did not work, it means the bands are somehow separated from their nscf.
        if nscfpk is None:
            print("Please help me with the pk of the nscf")
            return
        else:
            nscf = load_node(nscfpk)
            params = nscf.inputs.parameters.get_dict()
    #
    # charge corresponding to 1e13
    stru = nscf.inputs.structure
    area = stru.get_cell_volume()/stru.cell[2][2]  # angstroms
    n = 1e-3  # electrons per A^2 (correspondin to n= 10^13 electrons per cm^2)
    abs_charge = n*area
    default_charge = abs_charge if doping == 'holes' else -abs_charge
    #
    # edge
    HO, LU = get_HO_LU(nscf, bands)
    edge = HO if doping == 'holes' else LU
    #
    if 'tot_charge' in nscf.inputs.parameters.get_dict()['SYSTEM'].keys():
        charge = nscf.inputs.parameters.get_dict()['SYSTEM']['tot_charge']
        ef = nscf.outputs.output_parameters.dict.fermi_energy
        print(("found charge {} in nscf".format(charge)))
        if np.sign(charge*default_charge) < 0:
            print("charge does not correspond to specified doping, using dummy charge and Fermi level")
            charge = default_charge
            ef = edge + 0.1 if charge<0 else edge - 0.1
    else:
        print("nscf is neutral, using dummy charge and Fermi level")
        charge = default_charge
        ef = edge + 0.1 if charge<0 else edge - 0.1
    # override if ef_from_edge has been given
    if ef_from_edge is not None:
        ef = edge + ef_from_edge if charge<0 else edge - ef_from_edge

    # window edge = ef + 3*room temp + 3* max om
    wiedge = HO - (ef - 0.09 - 3*maxom) if charge > 0 else ef + 0.09 + 3*maxom - LU
    ef_from_edge = HO-ef if charge > 0 else ef-LU
    window = abs(wiedge) + 0.2
    # print("window", window, wiedge)
    #
    kpoints, equiv, transpbands, rots = get_transp_bands(nscf, bands, window, charge, mode='indices')
    structure = nscf.inputs.structure.get_ase()
    eks = np.concatenate(list(transpbands.values()))['energy']
    de = np.max(eks)-(np.min(eks)+wiedge)
    mesh = nscf.inputs.kpoints.get_kpoints_mesh()[0]
    vals = Valleys(structure,mesh, kpoints, equiv, transpbands, rots, charge, de,
    time_reversal=nscf.res.time_reversal_flag, rough_vel=False,
    noncolin=nscf.outputs.output_parameters.dict.non_colinear_calculation,
    optimize_BZ_shape=optimize_BZ)

    # Very silly fix for a specific system... WSe2h.
    # The q vectors were generated with
    # the K' valley at a different (G-equivalent) position with
    # respect to the bands
    # used to generate vals object... this fixes that...
    if bands.uuid == 'c4f77f32-9b5b-4d08-a157-3b58302497e2':
        idxs = np.argwhere(vals.kpoints[:, 1] < 0).flatten()
        vals.kpoints[idxs] = vals.kpoints[idxs] + vals.rec_cell[0]
    return vals

def quickValleys(bands, doping, ef_from_edge=0.1, window=0.5, nscf = None, optimize_BZ=True):
    """
    ValleysandInfo can be quite slow.... This is a faster version, without analysis.
    Just use standard Fermi level and energy window.
    A dummy value is set for the charge, so DO NOT use vals.charge for some calculation.
    Note, however, that this is not a problem for the functions inside the
    Valleys class since they just use the sign of the charge.
    """
    # In a normal case, the nscf is just the creator of the bands
    if nscf is None: nscf = bands.creator
    #print(nscf)
    #dummy charge
    charge = -0.01 if doping == "electrons" else 0.01
    #
    kpoints, equiv, transpbands, rots = get_transp_bands(nscf, bands, window,
                                                         charge, mode='indices')
    eks = np.concatenate(list(transpbands.values()))['energy']
    de = max(eks)-(min(eks)+window)
    structure = nscf.inputs.structure.get_ase()
    mesh = nscf.inputs.kpoints.get_kpoints_mesh()[0]
    vals = Valleys(structure, mesh,kpoints, equiv, transpbands, rots, charge, de,
                   time_reversal=nscf.res.time_reversal_flag, rough_vel=True,
                   noncolin=nscf.outputs.output_parameters.dict.non_colinear_calculation,
                   optimize_BZ_shape=optimize_BZ)
    return vals

def collect_EPC(vals,elphmats, kis,interp = True, pad = True ):
    """
    to get raw outputs, interp = False
    it will return qs, kis, ijs, elph, freqs
    elph[iq, iki, ij, imode]
    kis in cartesian coordinates.
    """
    from scipy.spatial import KDTree
    kistree = KDTree(kis)
    Nq = len(elphmats)
    Nki = len(kis)
    Nmode = elphmats[0].get_shape('matrix_elements')[-1]
    qs = np.zeros((Nq,3))
    freqs = np.zeros((Nq,Nmode))
    ijs = np.array( [ [i,j] for i in vals.FS.keys() for j in vals.FS.keys()], dtype=int)
    Nijs = ijs.shape[0]
    elph = np.zeros((Nq, Nki, Nijs, Nmode))
    #
    for iq,e in enumerate(elphmats):
        #'q_point', 'frequencies', 'band_indices', 'initial_states', 'matrix_elements' all in eV A
        ikis = np.array([kistree.query(ki)[1] for ki in e.get_array('initial_states')])
        qs[iq] = e.get_array('q_point')
        freqs[iq] = e.get_array('frequencies')
        # bands are a bit more messy to deal with in general...
        ijselph = e.get_array('band_indices')
        if np.all(ijselph == ijs):
            elph[iq, ikis] =  e.get_array('matrix_elements')
        else:
            found = [False]*ijs.shape[0]
            ijsidxs = np.zeros(ijs.shape[0], dtype = int)
            for a, ij in enumerate(ijs):
                for b,ijel in enumerate(ijselph):
                    if np.all(ij == ijel):
                        ijsidxs[a] = b
                        found[a] = True
            if not np.all(found):
                print("some bands not found! better to exclude elphmat {}, ijs = {}".format(e.pk, ijsidxs))
                print(ijselph, ijs)
            else:
                #print(ijsidxs)
                elph[iq, ikis] =  e.get_array('matrix_elements')[:,ijsidxs,:]
    qs = qs / Atobohr
    kis = kis / Atobohr
    # pad elph with nearest value?
    if pad:
        qtree = KDTree(qs)
        ij = 0
        for iki, ki in enumerate(kis):
            # find known q points
            known = np.argwhere(np.all(elph[:,iki, ij,:] != 0., axis=1)).flatten()
            # and the nearest neighbors
            dists, inds = qtree.query(qs[known], k=4)
            # pad the neighbors
            for iq, ind in zip(known, inds):
                for i in ind:
                    if i not in known:
                        elph[i,iki,:,:] = elph[iq,iki,:,:]
    if interp:
        vals.interp_EPC(qs, kis, ijs, elph, np.abs(freqs))
        return
    else:
        return qs, kis, ijs, elph, np.abs(freqs)

def collect_EPC_old_sampling(vals,elphmats, interp = True):
    """
    to get raw outputs, interp = False
    it will return qs, kis, ijs, elph, freqs
    elph[iq, iki, ij, imode]
    """
    # just a check for old format of EPC:
    if 'initial_states' not in elphmats[0].get_arraynames():
        raise ValueError("Wrong EPC format, probably the OLD one. Either change it or use collect_EPC_OLD ")
    kis = elphmats[0].get_array('initial_states')
    ijs = elphmats[0].get_array('band_indices')
    # Sanity checks: The initial states and band inidices should be the same for all calculations
    for x in elphmats[1:]:
        sameks = np.min(np.linalg.norm(x.get_array('initial_states') - kis, axis = 1)) < 1e-5
        sameijs = np.all(ijs == x.get_array('band_indices'))
        if not (sameks and sameijs):
            print("different initial states in elphmat [{}] from calculation [{}], discarding".format(x.pk, x.creator.pk))
            print("(to combine calculations at different kis, but with the same qs, use get_raw_data=True and concatenate)")
            print(np.min(np.linalg.norm(x.get_array('initial_states') - kis, axis = 1)))
            print(ijs, x.get_array('band_indices'))
            elphmats.remove(x)
            continue
    Nki, Nij, Nmode = elphmats[0].get_shape('matrix_elements')
    Nq = len(elphmats)
    qs = np.zeros((Nq, 3))
    freqs = np.zeros((Nq,Nmode))
    elph = np.zeros((Nq, Nki, Nij, Nmode))
    for iq,x in enumerate(elphmats):
        qs[iq] = x.get_array('q_point')
        elph[iq] =  x.get_array('matrix_elements')
        freqs[iq] = x.get_array('frequencies')
    qs = qs / Atobohr
    kis = kis[:,:3] / Atobohr
    if interp:
        vals.interp_EPC(qs, kis, ijs, elph, np.abs(freqs))
        return
    else:
        return qs, kis, ijs, elph, np.abs(freqs)

def get_metal_bands(nscf, bands, nbands = 2, mode = "simple"):
    #
    rots = reparse_qe_recip_syms(nscf)
    kpoints, equiv, kmap = checked_k_points_syms(nscf,bands, rots)
    #
    ebands = bands.get_bands()[kmap]
    #
    nelec=int(round(nscf.outputs.output_parameters.dict.number_of_electrons))

    if nscf.outputs.output_parameters.dict.non_colinear_calculation:
        n_fullbands=nelec
    else:
        n_fullbands=int(nelec/2)
    #
    #HO = np.max(ebands[:,n_fullbands-1])
    #LU = np.min(ebands[:,n_fullbands])
    #

    bands_idxs =range(n_fullbands-int(nbands/2), n_fullbands-int(nbands/2)+nbands)
    # create datatype for structured array made of integer indices and float energies
    dt = np.dtype([('index', np.int16), ('energy',np.float64)])
    #
    metalbands = {}
    for i in bands_idxs:
        dum = [(k, e[i]) for k,e in enumerate(ebands)]
        metalbands[i]=np.array(dum, dtype=dt)
    if mode == "simple":
        irridxs = np.where(equiv==np.arange(len(equiv)))[0]
        irrks = kpoints[irridxs]
        return {bdstr:np.array([np.array([irrks[k[0]][0], irrks[k[0]][1], k[1] ]) for k in ks]) for bdstr, ks in metalbands.items()}
    else:
        return kpoints,equiv, metalbands, rots

def save_valleys(filename, vals, epc):
    from ase.io import write
    import h5py
    from aiida.plugins import DataFactory

    # collect the EPC
    elphmats = epc.get_outgoing(node_class = DataFactory("array")).all_nodes()
    qs, kis, ijs, elph, freqs = collect_EPC(vals, elphmats, epc.inputs.init_ks.get_kpoints(cartesian = True), interp= False)
    #write structure in some ASE standard format
    write(filename+".xyz",vals.structure)

    ibnds = list(vals.FS.keys())
    with h5py.File(filename+".hdf5", "w") as f:
        f.create_dataset("kpoints", data=vals.kpoints)
        f.create_dataset("equiv", data=vals.equiv)
        f.create_dataset("mesh", data=vals.mesh)
        f.create_dataset("rots", data=np.array([r[0] for r in vals.rots]))
        f.create_dataset("trevs", data=np.array([r[1] for r in vals.rots]))
        f.create_dataset("charge", data=vals.charge)
        f.create_dataset("de", data=vals.de)
        f.create_dataset("ibnds", data=ibnds)
        for ibnd in ibnds:
            f.create_dataset("transpbands{}".format(ibnd), data=vals.bands[ibnd])
        f.create_dataset("qpoints", data=qs)
        f.create_dataset("initial_states", data=kis)
        f.create_dataset("ijs", data=ijs)
        f.create_dataset("elph", data=elph)
        f.create_dataset("freqs", data=freqs)
    return

def load_valleys(filename):
    import h5py
    from ase.io import read

    dt = np.dtype([('index', np.int32), ('energy',np.float64)])

    structure = read(filename+".xyz")

    f = h5py.File(filename+".hdf5", "r")
    kpoints = f["kpoints"][()]
    equiv = f["equiv"][()]
    mesh = f["mesh"][()]
    rots = [[rot, trev] for rot, trev in zip(f["rots"][()],f["trevs"][()])]
    charge = f["charge"][()]
    de = f["de"][()]
    ibnds = f["ibnds"][()]
    transpbands = {}
    for ibnd in ibnds:
        transpbands[ibnd] = np.array(f["transpbands{}".format(ibnd)], dtype = dt)
    qs = f["qpoints"][()]
    kis = f["initial_states"][()]
    ijs = f["ijs"][()]
    elph = f["elph"][()]
    freqs = f["freqs"][()]

    vals = Valleys(structure,mesh, kpoints, equiv, transpbands, rots, charge, de,
    time_reversal=True, rough_vel=False, noncolin = True, optimize_BZ_shape=True)

    return vals, qs, kis, ijs, elph, freqs
