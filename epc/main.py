import numpy as np
from scipy import interpolate
from scipy.spatial import Delaunay
from itertools import groupby
from sklearn.cluster import DBSCAN
import copy
from tqdm import tqdm
import json
import os
from .common import *


dt = np.dtype([('index', np.int32), ('energy',np.float64)])

class Valleys(object):
    """
    Valleys object to do all the pre and post processing associated
    to phonon-limited transport workflow.

    Inputs:
        structure: ASE structure of the material
        kpoints: 3*Nk array of k vectors in the BZ (outpout of QEstyle_grid or get_transp_bans or k_points_syms)

        equiv: QE-style integer array with the index of the equivalent irreducible kpoint for each kpoint in kpoints

        bands: dictionnary of band energies {band_number: array([ [k_index,k_energy] ,.., .. ], dtype = [('index', np.int32), ('energy',np.float64)] )}
               where k_index refers to the index of the irreducible kpoints:
               kpoints[np.argwhere(equiv==np.arange(len(kpoints)))]

        rots: QE style list of symmetry rotations for the k points, output of reparse_qe_recip_syms or get_qe_recip_syms
        [ [np.array([[1., 0., 0.],
                     [0., 1., 0.],
                     [0., 0., 1.]]), trev],
           [..]
         ]
        (in case of doubt, trev = 0)

        mesh: grid mesh for the kpoints [Nk1, Nk2, Nk3]

        charge: charge of the material in e/unit-cell

        de: energy margin to take away from bands

        time_reversal (default= True) : If one wants to take away time reversal sym

        optimize_BZ_shape (default = True) : Try to pack valleys at zone borders

        rough_vel (default = False) : if you don't need perfect velocities
    """
    def __init__(self,structure,mesh, kpoints,equiv,bands,rots,
    charge, de, time_reversal = True, optimize_BZ_shape=True, rough_vel = False, ef = None,
    noncolin = False):
        #
        # structure is an ASE str
        self.structure = structure
        # reciprocal cell
        # from ASE documentation of the get_reciprocal cell method:
        # "Note that the commonly used factor of 2 pi for Fourier transforms is not included here"
        # So we add it.
        self.rec_cell = structure.cell.reciprocal()*2*np.pi/Atobohr
        # twopioa
        alat = np.linalg.norm(structure.get_cell()[0])*Atobohr
        self.twopioa = 2*np.pi/alat
        self.rots = rots
        #
        self.time_reversal = time_reversal
        self.noncolin = noncolin
        # equiv is an array corresponding to the indices of the equivalent kpoints, QE-style
        self.equiv= equiv
        # kpoints is an array of kpoints in the first BZ
        # one gets the indices of the irreducible kpoints as follows:
        self.irridxs = np.where(equiv==np.arange(len(kpoints)))[0]
        # and the corresponding kpoints
        self.irrks = kpoints[self.irridxs]
        # bands is an dict of arrays containing the IBZ bands in a certain energy window
        self.bands = bands
        # number of k points in the BZ
        self.NBZ = len(equiv)
        self.SBZ = np.cross(self.rec_cell[0], self.rec_cell[1])[2]
        self.dkBZ=np.sqrt(self.SBZ/self.NBZ)
        self.mesh = mesh
        # charge of the material (tot_charge in QE)
        self.charge=charge
        # to shave off
        self.de=de
        #
        # initialize bands-related attributes
        # pockets, extrema, FS (final states), redexts are dictionaries
        # keys are of the form (ipock,ibnd), ipock running on different
        #         pockets in a given band ibnd, or just ibnd for FS.
        #values are arrays of dtype (index, energy) for each kpoints
        self.pockets, self.extrema, self.FS = self.get_pockets_and_extrema(self.de)
        self.kpoints, self.degeneracy, self.redexts = self.prelim_BZ_treatment(kpoints, self.extrema)
        #
        self.BZoptimized = False
        if optimize_BZ_shape :
            self.kpoints = self.optimize_BZ_shape(self.kpoints)
            self.BZoptimized = True
        #
        self.rough_vel = rough_vel
        #
        self.dv = None
        #
        self.kirr = None
        #
        if ef is not None:
            self.ef = ef
        self.allPkkps = {}

    def get_xye_coords(self,points):
        """
        points is an array of type (index, energy)
        return (kx, ky, energy)
        """
        return np.column_stack((self.kpoints[points['index'], :2], points['energy']))

    def get_pockets_and_extrema(self, shave_off):
        """
        From the bands crossed by the Fermi level, look at states within
        a certain energy window and cluster the points into pockets.
        Find the extrema (band edges) of each pocket.
        """
        pockets={}
        ibndmin, ibndmax = min(self.bands.keys()) , max(self.bands.keys())
        for ibnd, fullbnds in list(self.bands.items()):
            #shave off the top or bottom
            if self.charge <= 0:
                # indices of the ks in fullbnds that are below a certian energy
                # those indices run in the irreducible kpoints, not the full BZ
                bnds=np.where(fullbnds['energy']< max(self.bands[ibndmax]['energy'])-shave_off)[0]
            else:
                bnds=np.where(fullbnds['energy']> min(self.bands[ibndmin]['energy'])+shave_off)[0]
            # cluster into pockets
            if bnds.any():
                # Use DBSCAN to cluster the kpoints and find the pockets in the irreducible bands
                db = DBSCAN(eps=2*self.dkBZ, min_samples=3).fit(self.irrks[fullbnds[bnds]['index'],:2])
                # number of pockets found
                #N_pockets = len(set(db.labels_))
                #fill the irreducible pockets
                for i in set(db.labels_):
                    # with indices in irreducible kpoints
                    dum_ks = fullbnds[bnds[np.where(db.labels_ == i)[0]]]
                    #change indices to full BZ
                    dum_ks['index'] = self.irridxs[dum_ks['index']]
                    pockets[(i, ibnd)]= dum_ks
                # find their extrema
                if self.charge <= 0:
                    extrema={i:min(p, key= lambda x: x[1]) for i,p in list(pockets.items())}
                else:
                    extrema={i:max(p, key= lambda x: x[1]) for i,p in list(pockets.items())}
        # generate the Fermi Surface in the entire BZ
        # we collect the indices of the kpoints equivalent to the irr pockets in the BZ
        # FS = {band_index: [[k_index,energy] for k in expanded_pockets]}
        FS = {pstr[1]:[] for pstr in pockets.keys()}
        for pstr,p in pockets.items():
            for ik,e in p:
                fsidxs=np.where(self.equiv==ik)[0]
                FS[pstr[1]].extend([(ifs, e) for ifs in fsidxs])
        FS={ibnd: np.array(dum, dtype=dt) for ibnd, dum in FS.items()}
        return pockets, extrema, FS

    def prelim_BZ_treatment(self, kpoints, extrema):
        """
        reorganise the k points such that the pockets are inside and not separated.
        we also get the degeneracy of the pockets in the process
        bring back points to first BZ
        """
        BZ = bring_to_first_BZ(kpoints, self.rec_cell)
        # find all possible extrema in the BZ that are not G equivalents
        redexts = {}
        for extstr,(ik, e) in extrema.items():
            # initialize with the reference extrema
            re = [(ik,e)]
            # all equivalent points in the BZ
            eqidxs = np.where(self.equiv==ik)[0]
            #find the ones that are either the same or equivalent by G
            for ikeq in eqidxs:
                already_in = False
                G_equiv = False
                for ikr, e in re:
                    dk_cart = BZ[ikr] - BZ[ikeq]
                    if np.linalg.norm(dk_cart)<=2*self.dkBZ:
                        already_in = True
                    else:
                        dk = np.dot(np.linalg.inv(self.rec_cell.T), dk_cart)
                        if np.all(np.abs(dk-np.rint(dk))<=4*self.dkBZ):
                            G_equiv = True
                # if it is neither, it is an inequivalent (rotated version)
                if not already_in and not G_equiv: re.append((ikeq,e))
            redexts[extstr] = np.array(re,dtype=dt)
        degeneracy = {extstr: len(eqexts) for extstr, eqexts in list(redexts.items())}
        return BZ, degeneracy, redexts

    def pack_kpoints(self, BZ, extrema, pockets):
        """
        Non essential. In some cases, pockets on zone borders get split in two...
        This merges them back together.
        """
        # BZ is self.kpoints
        # get the size of the pockets to be able to determine if a point might be in there or not
        pocketssize = {pstr: max([np.linalg.norm(BZ[k[0]]-BZ[extrema[pstr][0]]) for k in p])
                      for pstr, p in list(pockets.items())}
        rec_cell = self.rec_cell
        Gs=[rec_cell[0],rec_cell[1], rec_cell[0]-rec_cell[1], rec_cell[0]+rec_cell[1]]
        # find out whether one ends up in a pocket if translating by some G vector. In that case, translate
        for G in Gs:
            for pstr in list(pockets.keys()):
                for ext, e in self.redexts[pstr]:
                    Geqs = np.where( np.linalg.norm(BZ+G-BZ[ext], axis=1)  <= pocketssize[pstr] + 2*self.dkBZ)
                    BZ[Geqs] = BZ[Geqs] +G
                    Geqs = np.where(np.linalg.norm(BZ-G-BZ[ext], axis=1) <= pocketssize[pstr] + 2*self.dkBZ)
                    BZ[Geqs] = BZ[Geqs] -G
        return BZ

    def optimize_BZ_shape(self,kpoints, pockindices=None):
        """
        Redefine the BZ such that all the valleys are surrounded by enough
        states, which is useful for the interpolation of velocities in particular
        """
        # first find the zone border extrema and the G vectors that should bring states close to them
        all_re = self.kpoints[np.concatenate(list(self.redexts.values()))['index']]
        Gp = [self.rec_cell[0],self.rec_cell[1], self.rec_cell[0]-self.rec_cell[1], self.rec_cell[0]+self.rec_cell[1]]
        allGs = Gp + [-G for G in Gp]
        zb = []
        Gs = []
        for G in allGs:
            # zone border extrema are those which have the same norm modulo G
            # the indices of the extrema fulfilling that criteria for this G are in ids
            ids = list(np.where(np.abs(np.linalg.norm(all_re, axis=1)-np.linalg.norm(all_re-G, axis=1)) < 5*self.dkBZ)[0])
            zb.extend(ids)
            # if this G corresponds to a zone border extrema, we store it
            if ids !=[]:
                Gs.append(G)
        # indices of all zone border extrema, removing possible duplicates.
        zb_exts = all_re[list(set(zb))]
        # here we bring back the zone border extrema in the positive quarter...

        # also remove duplicates for Gs
        Gs = remove_duplicates(Gs)
        # new BZ
        CZ = np.zeros((len(kpoints),3))
        if pockindices is None:
            pockindices = np.concatenate(list(self.pockets.values()))['index']

        for i,k in enumerate(kpoints):
            # in any case, we start by putting the same k in.
            CZ[i] = k
            # in some rare case, the following process destroys the pockets...
            # So I exclude the kpoints of the pockets from the process. Pockets shouldn't move anyhow
            if i not in pockindices:
                # distance from k to closest extrema
                dist = min([norm2D(k-re) for re in all_re])
                for G in Gs:
                    # distance from k+G to closest extrema
                    d = min([norm2D(k+G-re) for re in zb_exts])
                    # if k+G is now closer to an extrema, we replace the value
                    if d < dist:
                        CZ[i] = k+G
                        # but we update dist and continue in case an other G gets it even closer.
                        dist = d
                        continue
        return CZ

    def states_for_plot(self,states_str, keystr = None):
        """
        To quickly get a "plotable" array of states (kx, ky, energy)
        states_str: 'pocket' to plot the pockets, 'FS' to plot all final states
        """
        if states_str == 'pocket':
            states = self.pockets
        elif states_str == 'FS':
            states = self.FS
        else:
            print("unrecognized type of states... try pocket or FS")
        if keystr is None:
            print("No key specified, so I sum all the states. I.e., I sum on indices:", list(states.keys()))
            toplot = np.concatenate(list(states.values()))
        else:
            toplot = states[keystr]
        return np.column_stack((self.kpoints[toplot['index'],:2],toplot['energy']))

    def grad(self, fac, ibnd):
        """
        Compute the gradient of the fermi surface for given band index
        Use interpolated bands, with interpolation factor fac
        """
        # get the full band, not just valleys, otherwise you get a bunch of
        # issues at the boundaries
        fulleks = np.zeros(len(self.kpoints))
        for ik,k in enumerate(self.kpoints):
            fulleks[ik] = self.bands[ibnd][np.argwhere(self.irridxs == self.equiv[ik]).flatten()[0]]['energy']
        ks_cart = self.kpoints
        kxmin, kxmax = np.min(ks_cart[:,0]), np.max(ks_cart[:,0])
        kymin, kymax = np.min(ks_cart[:,1]), np.max(ks_cart[:,1])
        N=np.sqrt(self.NBZ)*fac
        Nx = 2*N / (1+ (kymax - kymin)/(kxmax - kxmin) )
        Ny = 2*N - Nx
        xk = np.linspace(kxmin, kxmax, int(Nx))
        yk = np.linspace(kymin, kymax, int(Ny))
        dx = xk[1]-xk[0]
        dy = yk[1]-yk[0]
        es = fulleks
        xx,yy = np.meshgrid(xk,yk)
        ek = interpolate.griddata(ks_cart[:,:2],es,(xx,yy),method='linear', fill_value=0.0)
        dey, dex = np.gradient(ek) # this is weird, I know, but this is due to the xy indexing convention in meshgrid
        vk = []
        for xl, yl, dexl,deyl, ekl in zip(xx, yy , dex , dey, ek):
            for xr, yr, dexr, deyr, ekr in zip(xl, yl, dexl,deyl, ekl):
                vk.append(np.array([xr,yr,dexr/dx,deyr/dy]))
        vk = np.array(vk)
        dvx = interpolate.griddata(vk[:,:2],vk[:,2],self.kpoints[self.FS[ibnd]['index'],:2], method='linear', fill_value=0.0)
        dvy =interpolate.griddata(vk[:,:2],vk[:,3],self.kpoints[self.FS[ibnd]['index'],:2], method='linear', fill_value=0.0)
        return np.array([dvx, dvy]).T

    def dv_is_wrong(self,dv, ibnd, fac):
        """
        the velocity is supposed to follow the symmetries of the band structure.
        When it doesn't, it's a sign something went wrong with the interpolation
        """
        FS = self.FS[ibnd]
        irr_in_fs = [i for i,idx in enumerate(FS['index']) if idx in self.irridxs]
        #
        wrong_dv = False
        for i in irr_in_fs:
            equiv_idxs = np.argwhere(self.equiv == FS[i]['index'])
            i_equivs = [np.argwhere(FS['index']==idx)[0][0] for idx in equiv_idxs]
            norms = np.linalg.norm(dv[i_equivs], axis = 1)
            if np.max(norms)-np.min(norms)>1.0:#0.1:
                if fac < 5:
                    wrong_dv = True
                    break
                else:
                    dv[i_equivs] = np.array([0.,0.])
        return wrong_dv

    def get_FS_gradient(self):
        #
        if not self.BZoptimized: self.kpoints = self.optimize_BZ_shape(self.kpoints)
        #
        dv={}
        for ibnd in list(self.FS.keys()):
            fac = 1
            dvdum = self.grad(fac,ibnd)
            # in case we are not interested in precise velocities, we just directly
            # put fac to 10 such that wrong_dv simply puts the problematic points to 0
            if self.rough_vel: fac = 10
            while self.dv_is_wrong(dvdum, ibnd, fac):
                fac += 1
                dvdum = self.grad(fac,ibnd)
                print("wrong dv, increasing number of kpoints for gradient, times", fac)
            dv[ibnd] = dvdum
        self.dv=dv
        return

    def get_coarser_pockets_and_FS(self,N1N2=None, pocks=None):
        """
        new FS and pockets found based on new grid defined by N1N2 = [N1, N2].
        """
        #
        if N1N2 is None:
            newFS={ibnd: np.column_stack((self.kpoints[dum['index'],:2],dum['energy'])) for ibnd, dum in list(self.FS.items())}
            newpocksout = {ke:np.column_stack((self.kpoints[v['index'],:2],v['energy'])) for ke,v in list(self.pockets.items())}
            return newpocksout , newFS
        #distance=self.dkBZ*fac
        ##[max( int(np.ceil(round(np.linalg.norm(b) / distance, 5))), 1) for b in self.rec_cell[:2]]
        N1, N2 = N1N2
        mesh = ([N1,N2,1], [0.,0.,0.])
        coarse_ks,coarse_equiv = QEstyle_grid(mesh,self.rots, self.rec_cell)
        coarse_irridxs = np.where(coarse_equiv==np.arange(len(coarse_ks)))[0]
        # pockets. We output them directly in the plottable format, not going through
        # the indices, so not QE style...
        newpocks={}
        if pocks is None:
            pocks= self.pockets
        for val, ks in pocks.items():
            # find the points in the convex hull  of the pocket:
            if len(ks)>5:
                pidxs = coarse_irridxs[ are_in_pocket( coarse_ks[coarse_irridxs,:2], self.kpoints[ks['index'],:2] )]
                ek = interpolate.griddata(self.kpoints[ks['index'],:2],ks['energy'],coarse_ks[pidxs,:2],method='linear')
                newpocks[val] = np.array([(ik,e) for ik, e in zip(pidxs,ek)], dtype = dt)#np.column_stack((pidxs,ek))
        # FS
        if self.BZoptimized:
            coarse_ks = self.optimize_BZ_shape(coarse_ks, pockindices = np.concatenate(list(newpocks.values()))['index'])
        #coarse_ks = self.optimize_BZ_shape(coarse_ks)
        newFS = {pstr[1]:[] for pstr in list(newpocks.keys())}
        for pstr,p in list(newpocks.items()):
            for ik,e in p:
                fsidxs=np.where(coarse_equiv==ik)[0]
                newFS[pstr[1]].extend([np.array([coarse_ks[ifs][0],coarse_ks[ifs][1], e]) for ifs in fsidxs])
        newFS={ibnd: np.array(dum) for ibnd, dum in list(newFS.items())}
        newpocksout = {ke:np.column_stack((coarse_ks[v['index'],:2],v['energy'])) for ke,v in list(newpocks.items())}
        #newFSout ={ke:np.column_stack((coarse_ks[v['index'],:2],v['energy'])) for ke,v in newFS.items()}
        return newpocksout , newFS

    def sampling(self, N1N2 = None, faci = 1, intra_inter = [1,1], tol=0.9, pockFS= None):
        """
        sample the initial states, final states and qpoints to go from one the the other.
        N1N2 = [N1, N2] : possibility to use a new grid mesh
        intra_inter = [2,3]: must be INTERGERS! those are factors to coarsen the
        grid of q vectors for intravalley or intervalley transitions.
        The bigger the factor, the less qpoints.
        output is initial states, final states, irreducible qpoints in CRYSTAL COORDINATES
        replaces previous get_qpoints function
        pockFS: to input customized pocket and FS... {ibnd:[...]} where ibnd is the lowest(for electrons)
        """
        def get_rc_coords( ks, rc):
            return np.array([ np.rint(np.linalg.solve(rc.T, k[:2])) for k in ks],dtype=int)
        # start with creating a new FS and pockest, based on new N1*N2 grid:
        # RG = Raw grid already in cryst coords, with equiv vector, useful for symmetry stuff
        if pockFS is None:
            newpocks, newFS = self.get_coarser_pockets_and_FS(N1N2=N1N2)
        else:
            newpocks, newFS = pockFS
        #
        if N1N2 is None:
            N1, N2 = self.mesh[:2]
        else:
            N1, N2 = N1N2
        #I will work in the basis of rc: the reciprocal vectors generating the grid
        rc = self.rec_cell[:2,:2].copy()
        rc[0] = rc[0]/N1
        rc[1] = rc[1]/N2
        #for sampling we don't care about bands... take lowest if electrons, highest if holes (to cover most kpoints)
        ibnd = min(self.FS.keys()) if self.charge <0 else max(self.FS.keys())
        # all kpoints of the FS in rc coordinates: nice because integer!
        FS = get_rc_coords(newFS[ibnd],rc)
        # cluster pockets and FS
        kiclust = []
        for ipock, pock in newpocks.items():
            if ipock[1] == ibnd:
                kiclust.append( np.array([ki for ki in get_rc_coords(pock,rc) if ki[0]%faci==0 and ki[1]%faci==0]) )

        #
        FSclust = cluster(FS, 1)
        # find q points
        qs = []
        for i,kicl in enumerate(kiclust):
            for j,FScl in enumerate(FSclust):
                # intravalley?
                fac = intra_inter[1]
                if are_in_pocket(kicl,FScl).shape[0] >0 :
                    fac = intra_inter[0]
                #
                for ki in kicl:
                    for kq in FScl:
                        q = kq-ki
                        if q[0]%fac==0 and q[1]%fac==0 and q[0]**2+q[1]**2 != 0. :
                            qs.append(q)
        qs = np.unique(qs, axis = 0)
        # return kpoints, qpoints, targetkpoints
        # dividing by N1, N2 should give me the cryst coordinates.
        # rc[0] = rec_cell[0]/N1
        kis = np.concatenate(kiclust).astype(float)
        kis[:,0]  = kis[:,0]/N1
        kis[:,1]  = kis[:,1]/N2
        kis = np.column_stack((kis, np.zeros(len(kis))))
        print("{} initial states in total".format(len(kis)))
        #
        tks = FS.astype(float)
        tks[:,0]  = tks[:,0]/N1
        tks[:,1]  = tks[:,1]/N2
        tks = np.column_stack((tks, np.zeros(len(tks))))
        print("{} potential final states in total".format(len(tks)))
        #
        qs = qs.astype(float)
        qs[:,0]  = qs[:,0]/N1
        qs[:,1]  = qs[:,1]/N2
        qs = np.column_stack((qs, np.zeros(len(qs))))
        print("{} (reducible) qpoints needed to go from initial to final".format(len(qs)))
        print("reducing qpoints...")
        fac = 0.5*(N1/self.mesh[0]+ N2/self.mesh[1])
        # bring the target qpoints to cartesian coordiantes for rotations
        targetqs =  np.array([np.dot(self.rec_cell.T, x) for x in qs])
        # now I need to reduce the qpoints...
        red_qs=[]
        rotqs=[]
        for q in targetqs:
            if rotqs==[]:
                red_qs.append(q)
                to_rotate = [q]
                if self.time_reversal : to_rotate.append(q*np.array([-1,-1,1]))
                rotqs.extend(rotate_states(to_rotate, self.rec_cell, [r[0] for r in self.rots]))
            else:
                if min([norm2D(np.array(q)-np.array(y)) for y in rotqs])>tol*fac*self.dkBZ:
                    red_qs.append(q)
                    to_rotate = [q]
                    if self.time_reversal : to_rotate.append(q*np.array([-1,-1,1]))
                    rotqs.extend(rotate_states(to_rotate, self.rec_cell, [r[0] for r in self.rots]))
        print("{} qpoints after symmetry reduction".format(len(red_qs)))
        # bring back to cryst_coord
        redqs = np.array([np.linalg.solve(self.rec_cell.T, q) for q in red_qs])
        print("output is initial states, final states, qpoints in CRYSTAL COORDINATES.")
        return kis, tks, redqs

    def interp_EPC(self,  qs, kis, ijs, elph, freqs):
        """
        interpolate the EPC, should follow collect_EPC in aiida_wrapper.
        """
        # interpolate the energy of the initial state, on each band
        ekis={}
        for ibnd, band in list(self.FS.items()):
            ekis[ibnd] = interpolate.griddata(self.kpoints[band['index'],:2],band['energy'],(kis[:,0],kis[:,1]),method='linear', fill_value=0.0)
        ############################
        nmodes = 3*len(self.structure.get_positions())
        ##########################
        g2s = {}
        oms = {}
        kirr = {}
        FSdum = {i:np.column_stack((self.kpoints[ks['index'],:2],ks['energy'])) for i,ks in list(self.FS.items())}
        dumpocks = {ke:np.column_stack((self.kpoints[v['index'],:2],v['energy'])) for ke,v in list(self.pockets.items())}
        for i,ki in tqdm(enumerate(kis), desc="Interpolate", position=0):
            # the point of the valleys closest to ki in each band
            kips =  [(ibnd, min(xks, key= lambda x: norm2D(x-ki))) for ibnd,xks in list(FSdum.items())]
            bdidxs = []
            # all the ki+q
            kqs=ki[:2]+qs[:,:2]
            # loop on bands
            for ibnd, kip in kips:
                #check that the initial state is in the valley for this band.
                # otherwise we don't use it!
                if norm2D(kip-ki)<1.2*self.dkBZ :
                    kirr[(i,ibnd)]=np.array([ki[0], ki[1],ekis[ibnd][i]])
                    g2s[(i,ibnd)]={}
                    oms[(i,ibnd)]={}
                    for jbnd, xfs in list(FSdum.items()):
                        g2s[(i,ibnd)][jbnd]=[]
                        oms[(i,ibnd)][jbnd]=[]
                        qFS = xfs[:,:2]-ki[:2]
                        for mode in range(nmodes):
                            g2 = elph[:,i,ijs.tolist().index([ibnd,jbnd]),mode]
                            om = freqs[:,mode]
                            g2FS = interpolate.griddata((kqs[:,0],kqs[:,1]),g2,(xfs[:,0],xfs[:,1]),method='linear', fill_value=0.0)
                            g2s[(i,ibnd)][jbnd].append(g2FS)
                            # Their are some q points on the new grid that connot be interpolated.
                            # using fill_value=0.0 ensures that the associated weight is zero )
                            omFS = interpolate.griddata((qs[:,0],qs[:,1]),om,(qFS[:,0],qFS[:,1]),method='linear', fill_value=10.0)
                            oms[(i,ibnd)][jbnd].append(omFS)
        self.g2s = g2s
        self.oms = oms
        clean_kirr = {}
        out = 0
        ins = 0
        for kstr, ki in list(kirr.items()):
            if ki[2] == 0:
                out += 1
            else:
                clean_kirr[kstr] = ki
                ins += 1
        print("{}/{} initial states ignored because outside of valleys".format(out, ins+out))
        self.kirr = clean_kirr
        return

    def proj_v_dir(self, dir):
        """
        Project velocity on E-field direction dir. dir must be unitary
        """
        if self.dv is None:
            self.get_FS_gradient()
        return {ibnd:[np.dot(v,dir)*eVtoRy for v in dvibnd] for ibnd, dvibnd in list(self.dv.items()) }

    def prepare_for_transport(self, Edir, ficEdir = "velocity"):
        """
        preliminary calculations for transport. Edir is efield direction. should
        be unitary.
        """
        # velocities projected on Edir
        if self.dv is None:
            self.get_FS_gradient()
        self.vFSedir = self.proj_v_dir(Edir)
        if self.kirr is None:
            print("Define the kis first! (Collect and interp EPC)")
            return
        kirr=self.kirr
        # velocities projected on kidir / vidir
        vFSi={}
        for kstr,ki in tqdm(list(kirr.items()), desc="velocities", position=0):
            pidx,ext = min([(i,x) for i,x in list(self.extrema.items()) if i[1]==kstr[1]], key=lambda x: norm2D(ki-self.kpoints[x[1][0]]))
            kiext=np.array([ki[0]-ext[0],ki[1]-ext[1]])
            if norm2D(kiext)<1e-6:
                # direction of ki is ill-defined. take arbitrary one, x:
                vFSi[kstr] = self.vFSedir
            else:
                if ficEdir == "velocity":
                    FSidx = np.argmin(np.linalg.norm(self.kpoints[self.FS[kstr[1]]['index']][:,:2]-ki[:2],axis=1))
                    vec =  self.dv[kstr[1]][FSidx]
                    fdir = vec/np.linalg.norm(vec)
                else:
                    fdir=kiext/norm2D(kiext)
                vFSi[kstr]=self.proj_v_dir(fdir)
        self.vFSi=vFSi
        # densetoinit map
        # indices of kpoints in the pockets
        pockidxs = np.unique(np.concatenate(list(self.pockets.values()))['index'])
        # closest kirr for each of them
        eqkirrs = []
        for idx in pockidxs:
            kirrstr, kirr = min([(i,ki) for i,ki in list(self.kirr.items())], key = lambda x: norm2D(x[1]-self.kpoints[idx]))
            eqkirrs.append(kirrstr[0])
        eqkirrs = np.array(eqkirrs, dtype = np.int64)
        # below we map the FS on the kirrs
        # fsx['index'] = indices of kpoints in FS[ibnd]
        # equiv[fsx['index']] = indices of equivalent pocket points
        # np.argwhere(pockidxs==i)[0][0] =  position of equivalent pocket point in pockidxs list
        # eqkirrs[....] : corresponding equivalent kirr
        self.dense2init = {ibnd:eqkirrs[np.array([np.argwhere(pockidxs==i)[0][0] for i in self.equiv[fsx['index']]])]
                        for ibnd, fsx in list(self.FS.items()) }
        return

    def interpolate_on_FS(self, ksin, FSdum):
        """
        interpolate a quantity from the irreducible states ksin to the fully
        Fermi surface FSdum
        """
        rotin = {}
        dumrots = []
        for r in self.rots:
            rot = r[0]
            rot[2,2]=1.0
            dumrots.append(rot)

        for ibnd, x in list(ksin.items()):
            to_rotate = copy.deepcopy(x)
            if self.time_reversal : to_rotate.extend([y*np.array([-1,-1,1]) for y in x])
            rot_kis = remove_duplicates(rotate_states(to_rotate, self.rec_cell, dumrots))
            eqkis = self.optimize_BZ_shape(rot_kis)
            rotin[ibnd] = eqkis
        
        outFS={i: interpolate.griddata(
            ([k[0] for k in rotin[i]],[k[1] for k in rotin[i]]),
            [k[2] for k in rotin[i]],(xfs[:,0],xfs[:,1]),
            method='linear', fill_value=0.0) for i, xfs in list(FSdum.items())}
        return outFS

    def taus_v2surf(self, tausv, kis, irrkeys, FSdum):
        """
        From a vector of the lifetimes (tau) indexed on the irreducible initial states to
        interpolated values on the Fermi surface
        """
        #tausv is a vector of tau indexed on kis = self.kirr.values()
        # we first transform it into a "surface" on the irreducible pockets of the form:
        # {pocket:[np.array([kx, ky, tau]) for k in in kirr]}
        tausIS = {pstr:[] for pstr in set([x[1] for x in irrkeys])}
        for k,kstr, tau in zip(kis,irrkeys, tausv):
            if np.isfinite(tau) and kstr not in self.small_vis : 
                tausIS[kstr[1]].append(np.array([k[0], k[1], tau]))
            else:
                print("{} excluded from interpolation because: tau infinite ({}) or small vi ({})".format(kstr, not np.isfinite(tau), kstr in self.small_vis))
        # then interpolate on FS
        # {ibnd: [np.array([kx, ky, tau]) for k in in FS]}
        tausFS = self.interpolate_on_FS(tausIS, FSdum)
        return tausIS, tausFS

    def iterate(self, kis, irrkeys, LHS, diags, offdiags, FSdum, closed_form):
        """
        this iterates on the solution of the BTE until self-consitency
        """
        # initial closed_form solution
        oldtaus = np.zeros(len(irrkeys))
        newtaus = np.zeros(len(irrkeys))
        # issue when ki is too far from anything and both LHS and denom are zero
        for kidx,(kstr,ki) in enumerate(zip(irrkeys, kis)):
            denom = diags[kidx] + sum([x[0] for x in offdiags[kidx]])
            oldtaus[kidx] = LHS[kidx]/denom
        if closed_form:
            # stop there, just interpolate on irr pockets then FS
            tausIS, tausFS = self.taus_v2surf(oldtaus, kis, irrkeys, FSdum)
        else:
            # iterate to final solution
            dist = 10
            i=0
            while dist>1e-2:
                # interp
                tausIS, tausFS = self.taus_v2surf(oldtaus, kis, irrkeys, FSdum)
                for kidx,(kstr,ki) in enumerate(self.kirr.items()):
                    offd = sum([x[0]*np.mean([tausFS[x[2]][iv] for iv in x[1]]) for x in offdiags[kidx]])
                    newtaus[kidx]=(LHS[kidx]-offd) / diags[kidx]
                dist = np.linalg.norm(newtaus-oldtaus)/np.linalg.norm(oldtaus)
                print(i, dist)
                i+=1
                oldtaus = newtaus.copy()
            tausIS, tausFS = self.taus_v2surf(newtaus, kis, irrkeys, FSdum)
        return tausIS, tausFS

    def resistivity(self, tausFS, FSdum, ef, kT):
        """
        compute the resistivity
        """
        degeneracy = 1 if self.noncolin else 2
        pref = degeneracy*self.SBZ/(2*np.pi**2)/self.NBZ/kT/eVtoRy
        # (spin degenracy)*e^2/(2pi)**2 * SBZ/NBZ /(kT*eV2Ry)
        sig=0.0
        #sigks = {} # for debugging
        for ibnd, xfs in list(FSdum.items()):
            #sigks[ibnd] = np.zeros(xfs.shape[0]) # debug
            for ik, (e, v, tau) in enumerate(zip(xfs[:,2],self.vFSedir[ibnd], tausFS[ibnd])):
                sigk = pref*v**2*tau*FD(ef,e, kT)*(1.0-FD(ef,e, kT))
                #sigks[ibnd][ik] = sigk # debug
                sig += sigk
        return 1./sig*RytoOhm#, sigks # debug

    def BTEline_elements(self, ki, kstr, vFSi, FSdum, idxmodes, small_vi):
        """
        Compute a given line of the BTE matrix
        """
        # computes the neccassary term for one "line" of the BTE.
        # that is, the equation in the system related to ki
        fac = 2.0*np.pi/self.SBZ*eVtoRy # now in Rydberg (eVtoRy**2 from g2, 1/eVtoRy from Gaussian) /6.582119e-16 /hbar for eV, s units
        #
        Pkkps =  []
        #
        for jbnd, xfs in list(FSdum.items()):
            kpts = xfs[:,:2]
            tri = Delaunay(kpts)
            for mode in idxmodes:
                g2s = self.g2s[kstr][jbnd][mode]
                omFS= self.oms[kstr][jbnd][mode]
                for sign in [1., -1.]:
                    dumFS = np.column_stack((xfs[:,:2], xfs[:,2]-ki[2]+ sign * omFS))
                    for i,vertices in enumerate(tri.simplices):
                        tri_energies = sorted([dumFS[iv][2] for iv in vertices])
                        if tri_energies[0] < 0 and tri_energies[2] > 0:
                            #
                            sif = sorted(vertices, key = lambda x: dumFS[x][2])
                            tri_kpts = np.array([ kpts[iv] for iv in vertices])
                            # compute the area of the triangle
                            k1 = tri_kpts[1] - tri_kpts[0]
                            k2 = tri_kpts[2] - tri_kpts[0]
                            area = abs(np.cross(k1,k2))
                            e10 = tri_energies[1] - tri_energies[0]
                            e20 = tri_energies[2] - tri_energies[0]
                            e21 = tri_energies[2] - tri_energies[1]
                            if 0 < tri_energies[1]:
                                g2 = g2s[sif[0]]-0.5*tri_energies[0]*((g2s[sif[1]]-g2s[sif[0]])/e10+(g2s[sif[2]]-g2s[sif[0]])/e20 )
                                v = vFSi[jbnd][sif[0]]-0.5*tri_energies[0]*((vFSi[jbnd][sif[1]]-vFSi[jbnd][sif[0]])/e10+(vFSi[jbnd][sif[2]]-vFSi[jbnd][sif[0]])/e20)
                                e = xfs[sif[0]][2]-0.5*tri_energies[0]*((xfs[sif[1]][2]-xfs[sif[0]][2])/e10+(xfs[sif[2]][2]-xfs[sif[0]][2])/e20)
                                Pfac = - area * tri_energies[0]/e10/e20
                            else:
                                g2 = g2s[sif[2]]-0.5*tri_energies[2]*((g2s[sif[2]]-g2s[sif[1]])/e21+(g2s[sif[2]]-g2s[sif[0]])/e20)
                                v = vFSi[jbnd][sif[2]]-0.5*tri_energies[2]*((vFSi[jbnd][sif[2]]-vFSi[jbnd][sif[1]])/e21+(vFSi[jbnd][sif[2]]-vFSi[jbnd][sif[0]])/e20)
                                e = xfs[sif[2]][2]-0.5*tri_energies[2]*((xfs[sif[2]][2]-xfs[sif[1]][2])/e21+(xfs[sif[2]][2]-xfs[sif[0]][2])/e20)
                                Pfac =  area * tri_energies[2] /e21/e20
                            if sign > 0:
                                Pkkp = Pfac * fac * g2
                            else :
                                Pkkp = Pfac * fac * g2
                            #if np.isnan(Pkkp): print(Pfac, fac, g2)
                            Pkkps.append([Pkkp, v, e , sif, jbnd])
        return Pkkps

    def ki_treatment(self,ki,kstr,avv, FSdum):
        """
        each line of the BTE (i.e. for each initial state) is defined differently.
        This deals with that aspect.
        """
        # find out if ki is a state with small velocity, like at the bottom of a band.
        # in that case, we do not use its velocity in the BTE.
        # define the velocities used (different for each ki, because projected on vi)
        small_vi=False
        val, ext = min(list(self.extrema.items()), key=lambda x: norm2D(ki-self.kpoints[x[1][0]]))
        kiext=np.array([ki[0]-ext[0],ki[1]-ext[1]])
        if norm2D(kiext)<1e-6:
            small_vi=True
            vi=0.0
            vFSi=self.vFSedir
        else :
            vFSi = self.vFSi[kstr]
            vi=vFSi[kstr[1]][np.array([norm2D(ki-x) for x in FSdum[kstr[1]]]).argmin()]
            # original value 1e-1 (10%)
            if abs(vi) < 1e-1*avv:
                vi = 0.0
                small_vi = True
                vFSi = self.vFSedir
        return val, vFSi, vi, small_vi

    def tauinvI(self,rho0, ef, kT, FSdum):
        """
        define an rough inverse relax time related to a disorder limited resistivity
        """
        rho0 = rho0*OhmtoRy
        if rho0 != 0.0:
            vF2av = float(sum([v**2*FD(ef,k[2], kT)*(1.0-FD(ef,k[2], kT)) for i in list(self.FS.keys()) for k,v in zip(FSdum[i], self.vFSedir[i]) ]))
            tauinvI = self.SBZ/np.pi**2/self.NBZ/kT/eVtoRy*vF2av*rho0
            print(tauinvI)
        else:
            tauinvI = 0.0
        return tauinvI

    def BTE_solution(self, ef, kT, rho0=0, closed_form=False, imodes=None, degeneracy = 2):
        """
        Solve the BTE!
        ef: Fermi level, eV
        kT: temperature, eV
        returns: resistivity in Ohms, lifetimes in Ry.
        """
        #
        FSdum = {i:np.column_stack((self.kpoints[ks['index'],:2],ks['energy'])) for i,ks in self.FS.items()}
        ########## Add a diagonal element for disorder
        tauinvI = self.tauinvI(rho0, ef, kT, FSdum)
        ################ Inialize some convenient variables
        kis = self.kirr.values()
        irrkeys = self.kirr.keys()
        Nkis = len(kis)
        nmodes = 3* len(self.structure.get_positions())
        avv=np.mean([abs(v) for v in np.concatenate(list(self.vFSedir.values()))])
        # this is if we want to solve on a selected set of modes
        if imodes == None:
            idxmodes = range(nmodes)
        else:
            idxmodes = imodes
        # Vectors associated to the BTE systems of equations
        LHS = np.zeros(Nkis)
        diags = np.zeros(Nkis)
        offdiags = []
        self.small_vis = []
        # loop on kirr states
        for kidx,(kstr,ki) in tqdm(enumerate(self.kirr.items()), desc="Building BTE", position=0):
            val, vFSi, vi, small_vi = self.ki_treatment(ki,kstr,avv, FSdum)
            #
            if kstr in self.allPkkps.keys():
                Pkkps = self.allPkkps[kstr]
            else:
                Pkkps = self.BTEline_elements(ki,kstr, vFSi, FSdum, idxmodes, small_vi)
                self.allPkkps[kstr] = Pkkps
            #####################
            F0i=FD(ef,ki[2], kT)
            if small_vi:
                lefthandside=(1-F0i)
                print("small_vi", kstr)
                self.small_vis.append(kstr)
            else:
                lefthandside=vi*(1-F0i)
            vinit = 1.0 if small_vi else vi
            diag = 0.0
            offdiag = []
            for x in Pkkps:
                occs = (1-FD(ef,x[2], kT))*boseEA(kT, ki[2]-x[2])
                diag += x[0]*vinit*occs
                if not small_vi:
                    offdiag.append([-x[0]*x[1]*occs, x[3], x[4]])
            ###################
            LHS[kidx] = lefthandside
            diags[kidx] = diag + tauinvI*vi/self.degeneracy[val]
            offdiags.append(offdiag)
        #
        tausIS, tausFS = self.iterate(kis, irrkeys, LHS, diags, offdiags,FSdum, closed_form)
        rho = self.resistivity(tausFS, FSdum, ef, kT)
        return rho, tausFS


# NEEDED by epc class
def bring_to_first_BZ(ks_in, rec_cell):
    ks_cart = np.copy(ks_in)
    Gs=[rec_cell[0],rec_cell[1], rec_cell[0]-rec_cell[1], rec_cell[0]+rec_cell[1]]
    for G in Gs:
        idxs = np.where(np.linalg.norm(ks_cart, axis=1)-np.linalg.norm(ks_cart-G, axis=1) > 1e-5)
        ks_cart[idxs] = ks_cart[idxs]-G
        idxs = np.where(np.linalg.norm(ks_cart, axis=1) - np.linalg.norm(ks_cart+G, axis=1) > 1e-5)
        ks_cart[idxs] = ks_cart[idxs]+G
    return ks_cart

def remove_duplicates(qs, prec=1e-4):
    """
    fast removal of duplicates in a list of vectors
    """
    ndig=1
    while prec*10**ndig<1.0:
      ndig+=1
    qs=sorted(qs, key=lambda x:round(x[0],ndig) )
    redqs=[]
    for kx, gx in groupby(qs, key=lambda x:round(x[0],ndig)):
      lgx=sorted(list(gx), key=lambda x:round(x[1],ndig))
      for ky, gy in groupby(lgx, key=lambda x:round(x[1],ndig)):
          lgy=list(gy)
          redqs.append(lgy[0])
    return redqs

def QE_BZ_iBZ(mesh, rots, time_reversal = True):
    """
    generate a BZ and irreducible BZ just like QE.
    """
    nk1, nk2, nk3 = mesh[0]
    k1, k2, k3 = mesh[1]
    #
    nkarr = np.array([nk1, nk2, nk3])
    karr = np.array([k1, k2, k3])
    #
    nk = nk1*nk2*nk3
    ks = np.zeros((nk,3))
    for i in range(nk1):
        for j in range(nk2):
            for k in range(nk3):
                n = k + j*nk3 + i*nk2*nk3
                ks[n][0] = float(i)/nk1+float(k1)/2/nk1
                ks[n][1] = float(j)/nk2+float(k2)/2/nk2
                ks[n][2] = float(k)/nk2+float(k3)/2/nk3
    #
    equiv=np.arange(nk)
    #
    for i, k in enumerate(ks):
        if equiv[i] == i:
            for ir, (rot, trev) in enumerate(rots):
                xkr = np.dot(rot, k)
                xkr = xkr - qe_nint(xkr) #np.array([x - int(round(x)) for x in xkr])
                #
                if trev==1: xkr = -xkr
                #
                vec = xkr * nkarr - 0.5*karr
                in_the_list = np.all(np.abs(vec-qe_nint(vec))<= 1e-5)#min([ abs(vec-int(round(x)))<= 1e-5 for x in vec])
                #
                if in_the_list :
                    idxs = [int(x) for x in  np.remainder(qe_nint(xkr * nkarr - 0.5*karr + 2* nkarr), nkarr)]
                    j = int(idxs[2] + idxs[1]*nk3 + idxs[0]*nk2*nk3)
                    if j>i and equiv[j]==j:
                        equiv[j] = i
                    else:
                        if equiv[j]!=i or j<i: print("something went wrong", j,i)
                if time_reversal:
                    vec = -xkr * nkarr - 0.5*karr
                    in_the_list = min(np.abs(vec-qe_nint(vec))<= 1e-5)#min([ abs(vec-int(round(x)))<= 1e-5 for x in vec])
                    #
                    if in_the_list :
                        idxs = [int(x) for x in  np.remainder(qe_nint(-xkr * nkarr - 0.5*karr + 2* nkarr), nkarr)]
                        j = int(idxs[2] + idxs[1]*nk3 + idxs[0]*nk2*nk3)
                        if j>i and equiv[j]==j:
                            equiv[j] = i
                        else:
                            if equiv[j]!=i or j<i: print("something went wrong", j,i)
    # Here, using the qe_nint below will get you exactly the same irr points as qe
    # but that is not something we want, because some points on zone
    # border will be separated from the rest of the wedge.
    #np.array([k - qe_nint(k) for k in ks])
    return ks, equiv

def QEstyle_grid(mesh,rots, rec_cell,time_reversal = True, put_in_first_BZ = False):
    #
    # mesh = [[N,N,1],[0.,0.,0.]]
    # rots =[[rot,0], ..]
    #
    ks, equiv = QE_BZ_iBZ(mesh,rots, rec_cell,time_reversal = time_reversal)
    #
    # Here we "slide to the first BZ" according to QE.
    # Actually, we're just centering the reciprocall cell around zero
    # This need to be done AFTER the above process of defining equiv,
    # otherwise I'm not sure we get the same result as QE
    ks = ks - np.rint(ks)
    #
    ks_cart = np.array([np.dot(rec_cell.T, x) for x in ks])
    if put_in_first_BZ:
        ks_cart = bring_to_first_BZ(ks_cart, rec_cell)
    #
    #We will  just give equiv in output, but this is how one can get the irr
    # states from equiv
    #irridxs = np.where(equiv==np.arange(nk))[0]
    #irrks = ks_cart[irridxs]
    return ks_cart, equiv

def are_in_pocket(ks, p):
    # returns indices of ks in the convex hull of p
    hull = Delaunay(p)
    return np.argwhere(hull.find_simplex(ks)!=-1).flatten()

def cluster(ks, dk, indices = False):
    db = DBSCAN(eps=2*dk, min_samples=3).fit(ks)
    N_pockets = len(set(db.labels_))
    #fill the clusters
    clusters=[]
    idxs = []
    for i in set(db.labels_):
        clusters.append(ks[np.where(db.labels_ == i)[0]])
        idxs.append(np.argwhere(db.labels_ == i).flatten())
    if indices:
        return idxs
    else:
        return clusters

def rotate_states(ks,rec_cell, rots):
    rot_ks=[]
    for rot in rots:
        for k in ks:
            rot_ks.append(np.dot(rot,np.linalg.solve( np.array(rec_cell).T , k)))
    return [k[0]*rec_cell[0]+k[1]*rec_cell[1]+k[2]*rec_cell[2] for k in rot_ks]
